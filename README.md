# Circontext

Translations *in* contexts.

Heavily based on the work of [Miguel
Grinberg](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world).

More info coming as soon as v0.0.1 has arrived or just heading to the
[milestones](https://gitlab.com/circontext/circontext/milestones) of the project.


Changelog/What have been done so far:

- [ ] Add license
- [ ] Implement 'Contexts'
- [ ] Deploy to heroku
- [X] Full-text search
- [X] Improve app structure
- [X] Multiple languages support
- [X] Timestamp handling (with javascrit)
- [X] E-mail support (reset password, send errors and logfiles)
- [X] Pagination of texts
- [X] Users can follow each other
- [X] Error handling (500, 404)
- [X] Profile page and avatars
- [X] Simple 'text' posting
- [X] Users: register, login, logout
- [X] SQLite
- [X] Templates and Sass files
