const locale = document.getElementById("locale").getAttribute("content");
const m = moment();

function formatDateFromNow(d) {
  moment.locale(locale);
  const date = moment(d.innerHTML).fromNow();
  return d.innerHTML = date;
}


if (document.getElementById("datetime")) {
  const userLastSeen = document.getElementById("datetime");
  formatDateFromNow(userLastSeen);
}

if (document.getElementsByClassName("text-timestamp")) {
  const textTimeStamp = document.getElementsByClassName("text-timestamp");

  for (i = 0; i < textTimeStamp.length; i++) {
    formatDateFromNow(textTimeStamp[i]);
  };
}
