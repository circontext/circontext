from datetime import datetime
from flask import render_template, flash, redirect, url_for, request, g, \
    current_app
from flask_login import current_user, login_required
from flask_babel import _, get_locale
from app import db
from app.main.forms import EditProfileForm, TextForm, SearchForm, ContextForm
from app.models import User, Text, Context
from app.main import bp


@bp.before_app_request
def before_request():
    if current_user.is_authenticated:
        current_user.last_seen = datetime.utcnow()
        db.session.commit()
        g.search_form = SearchForm()
    g.locale = str(get_locale())


@bp.route('/', methods=['GET', 'POST'])
@bp.route('/index', methods=['GET', 'POST'])
def index():
    return render_template("index.html", title=_('Home'))


@bp.route('/u/home', methods=['GET', 'POST'])
@login_required
def uhome():
    # ctext = [
    #     {
    #         'id': 1,
    #         'links': [2],
    #         'lang': 'pt',
    #         'context': 'busking',
    #         'author': {'username': 'srcr'},
    #         'body': 'A minha lona é do tamanho do céu, e a minha
    #           bilheteria é o meu chapéu'
    #     },
    #     {
    #         'id': 2,
    #         'links': [1],
    #         'lang': 'en',
    #         'context': 'busking',
    #         'author': {'username': 'lsrdg'},
    #         'body': 'My circus tent is as high as the sky and, the ticket
    #            office is my hat in the end'
    #     }
    # ]
    # return render_template('index.html', title='Home', ctext=ctext)
    form = TextForm()
    if form.validate_on_submit():
        text = Text(body=form.text.data, author=current_user)
        db.session.add(text)
        db.session.commit()
        flash(_('Your text is now live!'))
        return redirect(url_for('main.index'))

    page = request.args.get('page', 1, type=int)
    texts = current_user.followed_texts().paginate(
        page, current_app.config['TEXTS_PER_PAGE'], False)

    next_url = url_for('main.index', page=texts.next_num) \
        if texts.has_next else None
    prev_url = url_for('main.index', page=texts.prev_num) \
        if texts.has_prev else None
    return render_template("user_home.html", title=_('Home'), form=form,
                           texts=texts.items, next_url=next_url,
                           prev_url=prev_url)


@bp.route('/explore', methods=['GET', 'POST'])
@login_required
def explore():
    cform = ContextForm()
    form = TextForm()

    if cform.validate_on_submit():
        context = Context(contextname=cform.context.data)
        db.session.add(context)
        db.session.commit()
        flash(_('You have create a new Context!'))
        return redirect(url_for('main.explore'))

    contexts = Context.query.all()

    page = request.args.get('page', 1, type=int)
    texts = Text.query.order_by(Text.timestamp.desc()).paginate(
        page, current_app.config['TEXTS_PER_PAGE'], False)
    next_url = url_for('main.explore', page=texts.next_num) \
        if texts.has_next else None
    prev_url = url_for('main.explore', page=texts.prev_num) \
        if texts.has_prev else None
    return render_template('explore.html', title=_('Explore'),
                           form=form, cform=cform, texts=texts.items,
                           contexts=contexts,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/u/<username>')
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()

    page = request.args.get('page', 1, type=int)
    texts = user.texts.order_by(Text.timestamp.desc()).paginate(
        page, current_app.config['TEXTS_PER_PAGE'], False)
    next_url = url_for('main.user', username=user.username, page=texts.next_num) \
        if texts.has_next else None
    prev_url = url_for('main.user', username=user.username, page=texts.prev_num) \
        if texts.has_prev else None
    return render_template('user.html', user=user, texts=texts.items,
                           next_url=next_url, prev_url=prev_url)


@bp.route('/edit_profile', methods=['GET', 'POST'])
@login_required
def edit_profile():
    form = EditProfileForm(current_user.username)
    if form.validate_on_submit():
        current_user.username = form.username.data
        current_user.about_me = form.about_me.data
        db.session.commit()
        flash(_('Your changes have been saved.'))
        return redirect(url_for('main.edit_profile'))
    elif request.method == 'GET':
        form.username.data = current_user.username
        form.about_me.data = current_user.about_me
    return render_template('edit_profile.html', title='Edit Profile',
                           form=form)


@bp.route('/follow/<username>')
@login_required
def follow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash(_('User %(username)s not found.', username=username))
        return redirect(url_for('main.index'))
    if user == current_user:
        flash(_('You cannot follow yourself!'))
        return redirect(url_for('main.user', username=username))
    current_user.follow(user)
    db.session.commit()
    flash(_('You are following %(username)s!', username=username))
    return redirect(url_for('main.user', username=username))


@bp.route('/unfollow/<username>')
@login_required
def unfollow(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash(_('User %(username)s not found.', username=username))
        return redirect(url_for('main.index'))
    if user == current_user:
        flash(_('You cannot unfollow yourself!'))
        return redirect(url_for('main.user', username=username))
    current_user.unfollow(user)
    db.session.commit()
    flash('You are not following {}.'.format(username))
    flash(_('You are not following %(username)s.', username=username))
    return redirect(url_for('main.user', username=username))


@bp.route('/search')
@login_required
def search():
    if not g.search_form.validate():
        return redirect(url_for('main.explore'))
    page = request.args.get('page', 1, type=int)
    texts, total = Text.search(g.search_form.q.data, page,
                               current_app.config['TEXTS_PER_PAGE'])
    next_url = url_for('main.search', q=g.search_form.q.data, page=page + 1) \
        if total > page * current_app.config['TEXTS_PER_PAGE'] else None
    prev_url = url_for('main.search', q=g.search_form.q.data, page=page - 1) \
        if page > 1 else None
    return render_template('search.html', title=_('Search'), texts=texts,
                           next_url=next_url, prev_url=prev_url)
