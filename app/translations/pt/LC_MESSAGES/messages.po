# Portuguese translations for PROJECT.
# Copyright (C) 2019 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2019-03-08 12:00+0100\n"
"PO-Revision-Date: 2019-03-08 12:03+0100\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language: pt\n"
"Language-Team: pt <LL@li.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.6.0\n"

#: app/__init__.py:19
msgid "Please log in to access this page."
msgstr "Por favor entre com o seu nome de usuário para poder ter acesso à esta página"

#: app/forms.py:11 app/forms.py:18 app/forms.py:49
msgid "Username"
msgstr "Nome de usuário"

#: app/forms.py:12 app/forms.py:20 app/forms.py:42
msgid "Password"
msgstr "Senha"

#: app/forms.py:13
msgid "Remember Me"
msgstr "Lembre-se de mim"

#: app/forms.py:14 app/templates/login.html:4
msgid "Sign In"
msgstr "Entrar"

#: app/forms.py:19 app/forms.py:37
msgid "Email"
msgstr "Endereço eletrônico"

#: app/forms.py:22 app/forms.py:44
msgid "Repeat Password"
msgstr "Repetir senha"

#: app/forms.py:28 app/forms.py:61
msgid "Please use a different username."
msgstr "Por favor utilize um nome de usuário diferente."

#: app/forms.py:33
msgid "Please use a different email address."
msgstr "Por favor utilize um endereço eletrônico diferente."

#: app/forms.py:38 app/forms.py:45
msgid "Request Password Reset"
msgstr "Solicitar uma nova senha"

#: app/forms.py:50
msgid "About me"
msgstr "Sobre"

#: app/forms.py:51 app/forms.py:66
msgid "Submit"
msgstr "Submeter"

#: app/forms.py:65
msgid "Write a text"
msgstr "Escreva um texto"

#: app/routes.py:50
msgid "Your text is now live!"
msgstr "O seu texto foi adicionado!"

#: app/routes.py:88
msgid "Invalid username or password"
msgstr "Nome de usuário e/ou senha inválido(s)"

#: app/routes.py:114
msgid "Congratulations, you are now a registered user!"
msgstr "Parabéns, agora você está cadastrada!"

#: app/routes.py:128
msgid "Check your email for the instructions to reset your password"
msgstr "Confira no seu endereço eletrônico as instruções para criar uma nova senha"

#: app/routes.py:145
msgid "Your password has been reset."
msgstr "Sua nova senha foi criada."

#: app/routes.py:174
msgid "Your changes have been saved."
msgstr "Suas alterações foram salvas."

#: app/routes.py:188
#, python-format
msgid "User %(username)s not found."
msgstr "Usuária(o) %(username)s não foi encontrada(o)."

#: app/routes.py:191
msgid "You cannot follow yourself!"
msgstr "Você não pode seguir a você mesma(o)!"

#: app/routes.py:195
msgid "You are following %(username)!"
msgstr "Você está seguindo %(username)!"

#: app/routes.py:204
msgid "User %(username) not found."
msgstr "Usuária(o) %(username)s não foi encontrada(o)."

#: app/routes.py:207
msgid "You cannot unfollow yourself!"
msgstr "Você não pode deixar seguir você mesma(o)!"

#: app/routes.py:212
msgid "You are not following %(username)."
msgstr "Você não está seguindo %(username)."

#: app/templates/404.html:4
msgid "File Not Found"
msgstr "Arquivo não encontrado"

#: app/templates/404.html:5 app/templates/505.html:6
msgid "Back"
msgstr "Voltar"

#: app/templates/505.html:4
msgid "An unexpected error has occurred"
msgstr "Um erro inesperado ocorreu"

#: app/templates/505.html:5
msgid "The administrator has been notified. Sorry for the inconvenience!"
msgstr "O administrador foi notificado. Desculpe a inconveniência!"

#: app/templates/_text.html:10
#, python-format
msgid "%(username)s created "
msgstr "%(username)s criou "

#: app/templates/edit_profile.html:4
msgid "Edit Profile"
msgstr "Editar Perfil"

#: app/templates/index.html:4
msgid "Hi"
msgstr "Olá"

#: app/templates/index.html:33 app/templates/user.html:28
msgid "Newer texts"
msgstr "Textos mais recentes"

#: app/templates/index.html:36 app/templates/user.html:31
msgid "Older texts"
msgstr "Textos mais antigos"

#: app/templates/login.html:25
msgid "New User"
msgstr "Nov(a/o) Usuári(a/o)"

#: app/templates/login.html:25
msgid "Click to Register"
msgstr "Clique aqui para se registrar"

#: app/templates/login.html:27
msgid "Forgot Your Password"
msgstr "Esqueceu a senha"

#: app/templates/login.html:28
msgid "Click to Reset It"
msgstr "Clique para criar uma nova senha"

#: app/templates/navbar.html:3
msgid "Explore"
msgstr "Explore"

#: app/templates/navbar.html:5
msgid "Login"
msgstr "Entrar"

#: app/templates/navbar.html:7
msgid "Profile"
msgstr "Perfil"

#: app/templates/navbar.html:8
msgid "Logout"
msgstr "Sair"

#: app/templates/register.html:4
msgid "Register"
msgstr "Cadastre-se"

#: app/templates/reset_password.html:4
msgid "Reset Your Password"
msgstr "Recrie sua senha"

#: app/templates/reset_password_request.html:4
msgid "Reset Password"
msgstr "Criar nova senha"

#: app/templates/user.html:8
msgid "User"
msgstr "Usuári(a/o)"

#: app/templates/user.html:10
msgid "Last seen on"
msgstr "Vist(a/o) pela última vez em"

#: app/templates/user.html:11
msgid "followers"
msgstr "Seguidores"

#: app/templates/user.html:11
msgid "following"
msgstr "Seguindo"

#: app/templates/user.html:13
msgid "Edit your profile"
msgstr "Edite o seu perfil"

#: app/templates/user.html:15
msgid "Follow"
msgstr "Seguir"

#: app/templates/user.html:17
msgid "Unfollow"
msgstr "Deixar de seguir"

