��    5      �              l     m     �      �     �  <   �     �       /        C     P     b     h     p          �     �     �     �     �     �     �     �     �     �  "   �  %   "      H     i     q     z     �     �     �     �     �     �  A   �     !     *     /     K     h     q     ~  "   �     �     �     �          6  	   M  	   W  �  a     �     	     		     $	  N   +	     z	      �	  (   �	     �	     �	     
     
     !
     9
     @
     Q
  (   V
     
     �
     �
     �
     �
     �
     �
  P   �
  5   ?  0   u     �     �     �     �     �     �                 ;   #     _     p  0   }  0   �     �     �  !     &   #  (   J  -   s     �     �     �  
   �        %(username)s created  About me An unexpected error has occurred Back Check your email for the instructions to reset your password Click to Register Click to Reset It Congratulations, you are now a registered user! Edit Profile Edit your profile Email Explore File Not Found Follow Forgot Your Password Hi Invalid username or password Last seen on Login Logout New User Newer texts Older texts Password Please log in to access this page. Please use a different email address. Please use a different username. Profile Register Remember Me Repeat Password Request Password Reset Reset Password Reset Your Password Sign In Submit The administrator has been notified. Sorry for the inconvenience! Unfollow User User %(username) not found. User %(username)s not found. Username Write a text You are following %(username)! You are not following %(username). You cannot follow yourself! You cannot unfollow yourself! Your changes have been saved. Your password has been reset. Your text is now live! followers following Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-03-08 12:00+0100
PO-Revision-Date: 2019-03-08 12:03+0100
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: pt
Language-Team: pt <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.6.0
 %(username)s criou  Sobre Um erro inesperado ocorreu Voltar Confira no seu endereço eletrônico as instruções para criar uma nova senha Clique aqui para se registrar Clique para criar uma nova senha Parabéns, agora você está cadastrada! Editar Perfil Edite o seu perfil Endereço eletrônico Explore Arquivo não encontrado Seguir Esqueceu a senha Olá Nome de usuário e/ou senha inválido(s) Vist(a/o) pela última vez em Entrar Sair Nov(a/o) Usuári(a/o) Textos mais recentes Textos mais antigos Senha Por favor entre com o seu nome de usuário para poder ter acesso à esta página Por favor utilize um endereço eletrônico diferente. Por favor utilize um nome de usuário diferente. Perfil Cadastre-se Lembre-se de mim Repetir senha Solicitar uma nova senha Criar nova senha Recrie sua senha Entrar Submeter O administrador foi notificado. Desculpe a inconveniência! Deixar de seguir Usuári(a/o) Usuária(o) %(username)s não foi encontrada(o). Usuária(o) %(username)s não foi encontrada(o). Nome de usuário Escreva um texto Você está seguindo %(username)! Você não está seguindo %(username). Você não pode seguir a você mesma(o)! Você não pode deixar seguir você mesma(o)! Suas alterações foram salvas. Sua nova senha foi criada. O seu texto foi adicionado! Seguidores Seguindo 